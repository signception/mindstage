$(document).ready(function(){
    var position = function(){
        var wH = $(window).height();
        var wW = $(window).width();

        /**
         * Работаем с основным лэйаутом
         */
        var $ws = $('#rec-workspace');
        var workspaceHeight = $ws.getHeight();
        var workspacePaddingTop = parseInt($ws.parent().css('padding-top'));
        var workspacePaddingBottom = parseInt($ws.parent().css('padding-bottom'));

        $('#rec-body, #rec-left-column, #rec-right-column').height(
            workspaceHeight + workspacePaddingTop + workspacePaddingBottom
        );

        /**
         * Позиционируем аккордионы - растягиваем по высоте
         */
        $('.accordion').each(function(i, obj){
            var childHeight = 0;
            $.each($(obj).siblings(), function(ii, s){
                childHeight += $(s).getHeight();
            });

            // Выставляем высоту самим аккордеонам
            var accordionHeight = $(obj).parent().getHeight() - childHeight;
            $(obj).height( accordionHeight )

            // Выставляем высоту группам внутри них
            var titlesHeight = 0;
            $(obj).find('.accordion-title').each(function(ii, t){
                titlesHeight += $(t).getHeight();
            });

            $(obj)
                .find('.accordion-body').height(
                    accordionHeight - titlesHeight
                )
                .jScrollPane(); // Выставляем вертикальный скролл
        })
    }
    $(window).resize(position);
    $(document).ready(position);
});
