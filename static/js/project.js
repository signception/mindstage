$(document).ready(function(){
    $('#comment-textarea').focus(function(){
        $(this).next().show();
    });

    $(".subscribe").click(function(){
        var o = $('#subscribe-content');
        if (o.css('display') == 'none') {
            o.css({
                    'left': 'auto',
                    'top': 'auto',
                    'width': '120px',
                    'margin-left': '55px'
                })
                .show();
        } else {
            o.hide();
        }
    });
    $(".userblock").click(function(){
        var $o = $('#userblock-popover');
        if ($o.css('display') == 'none') {
            $o.css({
                    'left': 'auto',
                    'top': 'auto',
                    'width': '180px',
                    'margin': '45px 0 0 0px'
                })
                .show();
        } else {
            $o.hide();
        }
        //return false;
    });

    $('.item-menu-toggle').each(function(i, obj){
        $(obj).next().css({
            'left': $(obj).position().left - 112
        });
    });

    try {
        $('[data-title]').tooltip();
    } catch(e) { }

    /**
     * Плеер анимируем. Этот код вынести лучше куда-нибудь в правильное и доступное место,
     * чтобы он болтался тут. В ваш, например, js-ник
     */
    $('.speed').click(function(){
        if ( $('#player-speed-control').css('display') == 'none' ) {
            $(this).addClass('active');
            $('#player-speed-control').show();
        } else {
            $(this).removeClass('active');
            $('#player-speed-control').hide();
        }
    });

    /**
     * Таймер для плашки звука
     * @type {number}
     */
    var hovered = false;
    var timeoutOut = null;

    $('#player-volume-button').toggle(function(){
        $(this)
            .removeClass('player-icon-volume-normal')
            .addClass('player-icon-volume-none');
    }, function(){
        $(this)
            .removeClass('player-icon-volume-none')
            .addClass('player-icon-volume-normal');
    });

    $('#player-volume-button').hover(function(){
        if (!hovered) {
            $(this).addClass('active');
            $('#player-volume-control').show();
        }
        hovered = true;
        clearTimeout(timeoutOut);
    }, function(){
        $volume = $(this);

        timeoutOut = setTimeout(function(){
            $volume.removeClass('active');
            $('#player-volume-control').hide();

            clearTimeout(timeoutOut);
            hovered = false;
        }, 300);
    });



    $('.progress').click(function(ev){
        $('#progress-alert')
            .css({
                'left': ev.offsetX - 35
            })
            .fadeIn(200);
        timeoutOut = setTimeout(function(){
            $('#progress-alert').fadeOut();
        }, 1200);
    });


    /**
     * Показывание-сокрытие пароля в регистрации
     */
    $('.show-pass').toggle(function(){
        $(this).css('opacity', 0.5);

        var pswd = $("#password-input").val();
        $("#password-input").attr("id","password-input-2");
        $("#password-input-2").after( $("<input id='password-input' type='text' name='password'>") );
        $("#password-input-2").remove();
        $("#password-input").val( pswd );
    }, function(){
        $(this).css('opacity', 1);

        var pswd = $("#password-input").val();
        $("#password-input").attr("id","password-input-2");
        $("#password-input-2").after( $("<input id='password-input' type='password' name='password'>") );
        $("#password-input-2").remove();
        $("#password-input").val( pswd );
    });

    try {
        var $container = $('#masonry-container');
        $container.imagesLoaded(function(){
          $container.masonry({
            itemSelector : '.discover-item',
            columnWidth : 20
          });
        });
    } catch(e) { }


    /**
     * Аватарка — смена.
     */

    $('.change-avatar-link').toggle(function(){
        $('#change-avatar').show();
    }, function(){
        $('#change-avatar').hide();
    });
});
