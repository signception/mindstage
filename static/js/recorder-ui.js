$(document).ready(function(){
    /**
     * Тут делаем супер-мега-гениальные уникальные чекбоксы
     */
    $('input[type=checkbox]').each(function(i, obj){
        $(obj).hide();
        var $checkbox = $('<i class="input-checkbox"/>');

        $checkbox.click(function(){
            if ( $(this).hasClass('active') ) {
                $(this).removeClass('active');
                $(obj).attr('checked', false);
            } else {
                $(this).addClass('active');
                $(obj).attr('checked', true);
            }
        });
        $(obj).after($checkbox);
    });

    /**
     * Тут мы выставляем ширину у колбасок
     */
    var sumWidth = 0;
    $('.stages-block').each(function(){
        $(this).css('width',
            $(this).find('.stages-item').length * 110 + 2
        );
        sumWidth += $(this).width() + 12;
    });
    $('.stages__inner').width(
        sumWidth + $('.stages-action').width()
    );

    $('.line-item .part').live('click', function(){
        $('.line-item .part').removeClass('active');
        $(this).addClass('active');
    });


    /**
     * Всплывающее меню для Плэйхолдера
     */
    $('.rec-placeholder__panel__option').click(function(){
        if ( $('.rec-placeholder__panel__menu').css('display') == 'block' ) {
            $(this).removeClass('active');
            $('.rec-placeholder__panel__menu').hide()
        } else {
            $(this).addClass('active');
            $('.rec-placeholder__panel__menu').show()
        }
    });


    /**
     * Всплывающее меню дашборда с плашки
     */
    $('.dashboard-project-list .submenu-open').click(function(){
        var submenu = $(this).parent().find('.submenu');

        if ( submenu.css('display') == 'block' ) {
            $(this).removeClass('active');
            submenu.hide()
        } else {
            $(this).addClass('active');
            submenu.show()
        }
    });

    /**
     * Таймер для плашки под альфавидео
     * @type {number}
     */
    var hovered = false;
    var timeoutOut = null;

    // Тут мы автоматически скрываем плашку через 5 сек
    var placeholderTimer = setTimeout(function(){
        var plate = $('.rec-placeholder__panel');

        // Но только, если на плашку еще не наводили ручками
        if ( !hovered ) {
            plate.animate({'height': '5px'}, 400)
                 .addClass('collapsed')
        }
    }, 5000);

    // Обрабатываем mouseOver, mouseOut
    $('.rec-placeholder__panel').width(
        $('.rec-placeholder').width()
    );
    $('.rec-placeholder').hover(function(){
        // Если на плашку навели, и после события out флаг не сбросился,
        // то тогда и анимируем
        if (!hovered) {
            $('.rec-placeholder__panel')
                .animate({'height': '25px'}, 100)
                .removeClass('collapsed');
        }
        hovered = true;
        clearTimeout(timeoutOut);
    }, function(){
        // Магия. Таймаут нужен для того, чтобы плашка не мигала, когда мы
        // по ней елозим и субменю не пропадало
        timeoutOut = setTimeout(function(){
            $('.rec-placeholder__panel')
                .animate({'height': '5px'}, 100)
                .addClass('collapsed');
            clearTimeout(timeoutOut);
            hovered = false;
        }, 500);
    })


    $('.dashboard-project-list ul > li').hover(function(){}, function(){
        $(this).removeClass('active');
        $(this).find('.submenu').hide();
    });

    var readyCallback = function() {
        /**
         * Навешиваем скроллинг на сцены
         * @type {*|jQuery|HTMLElement}
         */
        var stagePane = $('.stages .stages__scroll')
        stagePane.jScrollPane({
            autoReinitialise: true,
            horizontalBarWidth: (
                parseInt($('.stages').width()) - parseInt($('.stages-row').width())
            ) + 40 + 'px',
            initCallback: function(pane) {
            }
        });



        /**
         * Навешиваем скроллинг на сцены
         * @type {*|jQuery|HTMLElement}
         */

        var timelinesPane = $('.timelines .lines')
        timelinesPane.jScrollPane({
            autoReinitialise: true,
            horizontalBarWidth: (
                parseInt($('.timelines').width()) - parseInt($('.timeline-row .tool-bar').width())
            ) - 50 + 'px',
            initCallback: function(pane) {
                $('.lines .jspDrag').on('mousedown', function(){
                    var $obj = $(this);
                    var testScroll = setInterval(function(){
                        if ( $obj.position().left != 0 ) {
                            $('.timelines .titles').addClass('shadow');
                        } else {
                            $('.timelines .titles').removeClass('shadow');
                        }
                    }, 50);
                })
            }
        });
    }

    $(document).ready(readyCallback);
    $(window).resize(readyCallback);
});
