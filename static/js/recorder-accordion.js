$(document).ready(function(){
    $('.accordion').each(function(i, obj){
        var $acc = $(obj);

        $acc.find('.accordion-title').live('click', function(ev){
            if ( ev.target.tagName === 'A' ) {
                return true;
            }

            if ( !$(this).parent().hasClass('active') ) {
                $acc.find('.accordion-body')
                    .slideUp(200)
                    .parent()
                    .removeClass('active');

                $(this)
                    .next()
                    .slideDown(200, function(){
                        $(this)
                            .show()
                            .jScrollPane();
                    })
                    .parent()
                    .addClass('active');
            }
        });
    })
});
