!function ($) {
    $.fn.getHeight = function (option) {
        var cssProp = [
            'margin-top',
            'margin-bottom',
            'padding-top',
            'padding-bottom',
        ]

        var actualHeight = 0;
        for (var i in cssProp) {
            actualHeight += parseInt( $(this).css( cssProp[i] ) );
        }

        actualHeight += $(this).height();

        return actualHeight;
    }
}(window.jQuery);